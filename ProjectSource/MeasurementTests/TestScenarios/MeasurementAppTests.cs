﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestHacks;

namespace Measurement.Tests
{
    [TestClass()]
    public class MeasurementAppTests : TestBase
    {
        private List<MeasComplexStruct> SampleData
        {
            get
            {
                return new List<MeasComplexStruct>
                {
                    new MeasComplexStruct() {alpha = 0.67863103365796018, beta = 1.0789388194945582, humidity = 0.5842968f, temperature =  37.403622720236584, value = 1.6353322474982983, valueCompens = 1.1973925060173341 },
                    new MeasComplexStruct() {alpha = 1.0179963596452788, beta = 0.99204641324195009, humidity = 0.824498f, temperature = 41.821860842743781, value = 2.1600096388160406, valueCompens = 2.5086018933745278 },
                    new MeasComplexStruct() {alpha = 0.93056643053204358, beta = 0.82926770306553221, humidity = 0.7144148f, temperature = 40.259578400259009, value = 1.7404761401725564, valueCompens = 1.343105746315427 },
                    new MeasComplexStruct() {alpha = 0.53140921992509593, beta = 1.8986307502571491, humidity = 0.345463872f, temperature = 38.879605096653023, value = 2.2646188004536913, valueCompens = 2.2848868803699287 },
                    new MeasComplexStruct() {alpha = 0.78787713410782445, beta = 1.1958521934288453, humidity = 1.27084124f, temperature = 60.411251884380334, value = 1.4289501066705181, valueCompens = 1.5482850006903932 },
                    new MeasComplexStruct() {alpha = 0.37357423396486922, beta = 0.69157779804214048, humidity = 0.300006777f, temperature = 40.738705908510994, value = 1.3452320641263971, valueCompens = 0.34754829912311552 },
                    new MeasComplexStruct() {alpha = 0.06087836560179305, beta = 0.25964920743226017, humidity = 0.742081463f, temperature = 37.152062585410228, value = 2.4753583732814457, valueCompens = 0.039128037774639884 },
                    new MeasComplexStruct() {alpha = 1.1926336123652641, beta = 0.61032135401139365, humidity = 0.04270125f, temperature = 30.921263279981865, value = 2.00043160669066, valueCompens = 1.0192655790572536 },
                    new MeasComplexStruct() {alpha = 1.0880997260280607, beta = 0.71887296776636533, humidity = 0.229165614f, temperature = 49.863561769465548, value = 2.132490096490494, valueCompens = 1.1676318065830138 },
                    new MeasComplexStruct() {alpha = 1.7222441915189573, beta = 1.3097342527710041, humidity = 0.8057561f, temperature = 30.255767438550347, value = 2.1724471666974075, valueCompens = 5.6354029881688259 }
                };
            }
        }

        [TestMethod()]
        public void IOErrorTest()
        {
            MeasurementApp measurementApp = new MeasurementApp(new GaussDevice());
            measurementApp.Connection = false;
            measurementApp.Measurement();
            Assert.IsTrue(measurementApp.IOError);
        }

        [TestMethod()]
        public void LastAverageValueTest()
        {
            MeasurementApp measurementApp = new MeasurementApp(new GaussDevice());
            measurementApp.measComplexStructList = SampleData;
            measurementApp.MeasurementInterval();
            Assert.AreEqual(measurementApp.LastAverageValue, 1, 42708971394137);
        }

        [TestMethod()]
        public void IsValidTest()
        {
            MeasurementApp measurementApp = new MeasurementApp(new GaussDevice());
            measurementApp.measComplexStructList = SampleData;
            measurementApp.MeasurementInterval();
            Assert.IsTrue(measurementApp.IsValid);
        }

        [TestMethod()]
        public void MeasurementIntervalTest()
        {
            MeasurementApp measurementApp = new MeasurementApp(new GaussDevice());
            measurementApp.measComplexStructList = SampleData;
            measurementApp.MeasurementInterval();
            Assert.AreEqual(measurementApp.LastAverageValue, 1, 42708971394137);
        }
    }
}