﻿namespace Measurement
{
    public interface IDevice
    {
        IDevice GetDevice();
        IMeasurement Measure();

        float GetHumidity();
        double GetAlpha();
        double GetBeta();
    }
}
