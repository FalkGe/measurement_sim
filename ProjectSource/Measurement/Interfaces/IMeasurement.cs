﻿namespace Measurement
{
    public interface IMeasurement
    {
        double Temperature { get; set; }
        double Value { get; set; }
    }
}