﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace Measurement
{
    public class Simulator : INotifyPropertyChanged
    {
        public MeasurementApp measurementApp;
        private bool _cancel;
        private int _interval = 500;

        public Simulator(MeasurementApp measurementApp)
        {
            this.measurementApp = measurementApp;
        }

        public int Interval
        {
            get { return _interval; }
            set
            {
                _interval = value;
                OnPropertyChanged("Interval");
            }
        }

        public void RunSimulation()
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            bool test = cts.IsCancellationRequested;
            CancellationToken ct = cts.Token;
            measurementApp.chartCollection.Clear();
            Task task = Task.Run(() =>
            {
                while (!ct.IsCancellationRequested)
                {
                    for (int i = 0; i < (_interval / 5); i++)
                    {
                        Thread.Sleep(5);
                        measurementApp.Measurement();
                    }
                    if (_cancel)
                    {
                        cts.Cancel();
                        _cancel = false;
                    }
                    measurementApp.MeasurementInterval();
                }
            }, ct);
        }

        public void StopSimulation()
        {
            _cancel = true;
        }


        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}
