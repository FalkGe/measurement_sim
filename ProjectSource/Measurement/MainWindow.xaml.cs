﻿using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;

namespace Measurement
{
    public partial class MainWindow : Window
    {
        Simulator simulator = new Simulator(new MeasurementApp(new GaussDevice()));

        public MainWindow()
        {
            InitializeComponent();
            GridPanel.DataContext = simulator.measurementApp;
            sliderInterval.DataContext = simulator;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            simulator.RunSimulation();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            simulator.StopSimulation();
        }

        private void btnReview_Click(object sender, RoutedEventArgs e)
        {
            ((LineSeries)chartValues.Series[0]).ItemsSource = null;
            ((LineSeries)chartValues.Series[0]).ItemsSource = simulator.measurementApp.chartCollection;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}