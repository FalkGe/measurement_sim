﻿using System;

namespace Measurement
{
    public class GaussDevice : IDevice
    {
        private readonly Random _rnd = new Random(0);

        public IDevice GetDevice()
        {
            return null;
        }

        public IMeasurement Measure()
        {
            var meas = new GaussMeasure { Value = _rnd.NextGauss(2, 0.5), Temperature = _rnd.NextGauss(40, 10) };
            return (IMeasurement)meas;
        }

        public float GetHumidity()
        {
            return (float)_rnd.NextGauss(0.5, 0.4);
        }

        public double GetAlpha()
        {
            return _rnd.NextGauss(1, 0.5);
        }

        public double GetBeta()
        {
            return _rnd.NextGauss(1, 0.5);
        }

        private class GaussMeasure : IMeasurement
        {
            public double Temperature { get; set; }
            public double Value { get; set; }
        }
    }
}
