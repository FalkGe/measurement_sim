﻿using System;

namespace Measurement
{
    public static class RandomExtensions
    {
        public static double NextGauss(this Random random, double mean, double std)
        {
            var uni1 = random.NextDouble();
            var uni2 = random.NextDouble();

            var randStdNormal = Math.Sqrt(-2.0 * Math.Log(uni1))
                                    * Math.Sin(2.0 * Math.PI * uni2);
            var randNormal = mean + std * randStdNormal;

            return randNormal;
        }
    }
}
