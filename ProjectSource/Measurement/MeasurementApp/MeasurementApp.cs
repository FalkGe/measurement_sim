﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace Measurement
{
    public class MeasurementApp : INotifyPropertyChanged
    {
        public List<MeasComplexStruct> measComplexStructList = new List<MeasComplexStruct>();
        private FilterStruct filter = new FilterStruct();
        private IDevice device;
        private IMeasurement measurement;
        private double _lastAverageValue;
        private bool _isValid;
        private bool _ioError;
        private bool _connection;
        private bool _alphaCompActive;
        private bool _betaCompActive;
        private bool _humidityCompActive;
        public Dictionary<DateTime, double> chartCollection = new Dictionary<DateTime, double>();


        public MeasurementApp(IDevice device)
        {
            this.device = device;
            _connection = true;
            _alphaCompActive = true;
            _betaCompActive = true;
            _humidityCompActive = true;
        }

        #region Properties

        public double FilterHigh
        {
            get { return filter.highest; }
            set
            {
                filter.highest = value;
                OnPropertyChanged("FilterHigh");
            }
        }

        public double FilterLow
        {
            get { return filter.lowest; }
            set
            {
                filter.lowest = value;
                OnPropertyChanged("FilterLow");
            }
        }

        public bool IOError
        {
            get { return _ioError; }
            set
            {
                _ioError = value;
                OnPropertyChanged("IOError");
            }
        }

        public bool Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        public double LastAverageValue
        {
            get { return _lastAverageValue; }
            set
            {
                _lastAverageValue = value;
                OnPropertyChanged("LastAverageValue");
            }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                OnPropertyChanged("IsValid");
            }
        }

        public bool AlphaCompActive
        {
            get { return _alphaCompActive; }
            set
            {
                _alphaCompActive = value;
                OnPropertyChanged("AlphaCompActive");
            }
        }

        public bool BetaCompActive
        {
            get { return _betaCompActive; }
            set
            {
                _betaCompActive = value;
                OnPropertyChanged("BetaCompActive");
            }
        }

        public bool HumidityCompActive
        {
            get { return _humidityCompActive; }
            set
            {
                _humidityCompActive = value;
                OnPropertyChanged("HumidityCompActive");
            }
        }

        #endregion // Properties

        public void Measurement()
        {
            try
            {
                if (Connection == false)
                {
                    throw new IOException();
                }
                measurement = device.Measure(); measComplexStructList.Add(new MeasComplexStruct() { value = measurement.Value, temperature = measurement.Temperature, alpha = device.GetAlpha(), beta = device.GetBeta(), humidity = device.GetHumidity() });
            }
            catch (IOException)
            {
                IOError = true;
            }
        }

        public void MeasurementInterval()
        {
            Helper helper = new Helper(measComplexStructList);
            helper.Validation();
            helper.Compensation(AlphaCompActive, BetaCompActive, HumidityCompActive);
            helper.Average(filter);
            LastAverageValue = helper.AverageValue;
            if (chartCollection.Count > 200)
            {
                chartCollection.Clear();
            }
            chartCollection.Add(DateTime.Now, _lastAverageValue);
            IsValid = helper.IsValid;
            measComplexStructList.Clear();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }
}


