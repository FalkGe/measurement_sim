﻿namespace Measurement
{
    public class MeasComplexStruct
    {
        public double value, temperature, alpha, beta, valueCompens;
        public float humidity;
    }

    public class FilterStruct
    {
        public double highest = 4.0, lowest = 0.3;
    }
}
