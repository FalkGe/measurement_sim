﻿using System.Collections.Generic;

namespace Measurement
{
    public class Helper
    {
        private List<MeasComplexStruct> _measComplexStructList;
        private bool _isValide;

        public Helper(List<MeasComplexStruct> measComplexStructList)
        {
            _measComplexStructList = measComplexStructList;
        }

        public bool IsValid
        {
            get { return _isValide; }
            set
            {
                _isValide = value;
            }
        }

        public double AverageValue { get; private set; }

        #region Methods

        public void Validation()
        {
            int anz = 0;
            foreach (var item in _measComplexStructList)
            {
                anz = (item.temperature > 50.0) ? anz + 1 : anz;
            }
            _isValide = (anz < (_measComplexStructList.Count / 2)) ? true : false;
        }

        public void Compensation(bool alphaCompActive, bool betaCompActive, bool humidityCompActive)
        {
            foreach (var item in _measComplexStructList)
            {
                item.valueCompens = item.value;
                item.valueCompens = alphaCompActive ? HumidityCompensation(item.value, item.humidity) : item.valueCompens;
                item.valueCompens = betaCompActive ? AlphaCompensation(item.valueCompens, item.alpha) : item.valueCompens;
                item.valueCompens = humidityCompActive ? BetaCompensation(item.valueCompens, item.beta) : item.valueCompens;
            }
        }

        public void Average(FilterStruct filter)
        {
            double sum = 0;
            int anz = 0;
            foreach (var item in _measComplexStructList)
            {
                if (item.valueCompens < filter.highest && item.valueCompens > filter.lowest)
                {
                    anz++;
                    sum += item.valueCompens;
                }
            }
            if (anz != 0)
            {
                AverageValue = sum / anz;
            }
            else
            {
                AverageValue = 0;
                IsValid = false;
            }
        }

        public static double AlphaCompensation(double value, double alpha)
        {
            return value * alpha;
        }

        public static double BetaCompensation(double alphaCompensation, double beta)
        {
            return alphaCompensation * beta;
        }

        public static double HumidityCompensation(double value, float humidity)
        {
            if (humidity < 0.25)
            {
                return value * 0.7;
            }
            else if (humidity > 0.75)
            {
                return value * 1.15;
            }
            return value;
        }

        #endregion
    }
}